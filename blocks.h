//-------------------------------------------------------------------------
// symbols 
//-------------------------------------------------------------------------

#define CORNER_TL	81
#define CORNER_TR	69
#define CORNER_BL	91
#define CORNER_BR	95
#define BOXLINE_VL	87
#define BOXLINE_VLF	85
#define BOXLINE_VLL	89
#define BOXLINE_VR	88
#define BOXLINE_VRF	86
#define BOXLINE_VRL	90
#define BOXLINE_HT1	82
#define BOXLINE_HT2	83
#define BOXLINE_HB1	92
#define BOXLINE_HB2	93

#define C_EMPTY		0
#define C_ROCK		27
#define C_ROCK2		123
#define C_SKULL		124
#define C_BUSH		96
#define C_BUSH1		125
#define C_BUSH2		126
#define C_FRUIT		3
#define C_FRUIT1	4
#define C_FRUIT2	5
#define C_CLOCK		62
#define C_DUCK		63

const char rocks[2] = {
C_ROCK,C_ROCK2
};


const char fruits[3] = {
C_FRUIT,C_FRUIT1,C_FRUIT2
};

const char bushes[3] = {
C_BUSH,C_BUSH1,C_BUSH2
};

#define SNAIL_N1	8
#define SNAIL_N2	9
#define SNAIL_E1	31
#define SNAIL_E2	15
#define SNAIL_W1	60
#define SNAIL_W2	59
#define SNAIL_S1	6
#define SNAIL_S2	32
#define STAIL_E		10
#define STAIL_W		61

const char snail[2][4] = {
	{SNAIL_N1,SNAIL_E1,SNAIL_S1,SNAIL_W1},
	{SNAIL_N2,SNAIL_E2,SNAIL_S2,SNAIL_W2}
};

#define MISSLE		127
