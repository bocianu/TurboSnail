#ifndef DL_H
#define DL_H

#pragma data-name (push, "DLI")
char dl1[] = { DL_BLK8, DL_BLK8, DL_BLK8, 
			  
			  DL_CHR40x8x1 | DL_LMS, 0,0,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1 | DL_DLI, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1 | DL_DLI, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1,  
			  
			  DL_JVB, 0, 0 
};
char dl2[] = { DL_BLK8, DL_BLK8, DL_BLK8, 
			  
			  DL_CHR40x8x1 | DL_LMS, 0,0,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1,
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
			  DL_CHR40x8x1, 
			  DL_CHR40x8x1,
              DL_CHR40x8x1,
			  DL_CHR40x8x1,
              DL_CHR40x8x1, 
			  DL_CHR40x8x1,  
			  
			  DL_JVB, 0, 0 
};
#pragma data-name (pop)

void dli1(void);
    
void initDLI_title(void){
    POKE(SDMCTL,0);                                 // turn off antic
    dl1[4] = (unsigned char) (PEEKW(SAVMSC) % 256);  //set video adress
    dl1[5] = (unsigned char) (PEEKW(SAVMSC) / 256);
    dl1[sizeof(dl1)-2] = ((unsigned) &dl1) % 256;      //set dl addres for jump
    dl1[sizeof(dl1)-1] = ((unsigned) &dl1) / 256;
    POKE(NMIEN,0xC0);                               // enable dli + vbi
    POKEW(SDLSTL,(unsigned int) &dl1);               // new dlist address
    POKEW(VDSLST,(unsigned int) &dli1);             // set dli vector
    POKE(SDMCTL,34);                                // turn on antic
}


void initDLI_normal(void){
    POKE(SDMCTL,0);                                 // turn off antic
    dl2[4] = (unsigned char) (PEEKW(SAVMSC) % 256);  //set video adress
    dl2[5] = (unsigned char) (PEEKW(SAVMSC) / 256);
    dl2[sizeof(dl2)-2] = ((unsigned) &dl2) % 256;      //set dl addres for jump
    dl2[sizeof(dl2)-1] = ((unsigned) &dl2) / 256;
    POKE(NMIEN,0xC0);                               // enable dli + vbi
    POKEW(SDLSTL,(unsigned int) &dl2);              // new dlist address
    POKE(SDMCTL,34);                                // turn on antic
}

#endif