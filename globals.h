#ifndef GLOBALS_H
#define GLOBALS_H

// version

#define VER_H 1
#define VER_L 32

// game states/screens
#define GAME_TITLE      0
#define GAME_START      1
#define GAME_HELP       2
#define GAME_HALL_OF    3
#define GAME_EXIT       99

#define ATTRACT 0x4D

#define PLAYFIELD_WIDTH 26
#define PLAYFIELD_HEIGHT 22
#define PLAYFIELD_SIZE PLAYFIELD_HEIGHT*PLAYFIELD_WIDTH
#define PLAYFIELD_MIDH (PLAYFIELD_HEIGHT>>1)
#define PLAYFIELD_MIDL PLAYFIELD_MIDH-1
#define SCREEN_SIZE 40

// colors 
#define SCORES_UNDERGOAL 0x10
#define SCORES_GOAL 0xD0
#define BOXBG_LEVELSTART 0X90
#define BOXBG_GAMEOVER   0X20
#define BOXBG_WELLDONE   0XC0

// sounds
#define SFX_CLOCK   11
#define SFX_SKULL   7
#define SFX_BUSH    5
#define SFX_FRUIT   4
#define SFX_DUCK    1
#define SFX_SHOOT   6
#define SFX_MOVE    8
#define SFX_GOAL    9
#define SFX_NOGOAL  10

// helpers
#define putC(x,y,a) video_ptr[(x)+(y)*SCREEN_SIZE]=(a)
#define getRTC ((PEEK(18)*65536+PEEK(19)*256+PEEK(20))/60)
#define clearScr memset(video_ptr,0,40*24)
//#define clearScr _graphics(0)

unsigned char   playfield[PLAYFIELD_HEIGHT][PLAYFIELD_WIDTH],
playfieldFruits[PLAYFIELD_HEIGHT][PLAYFIELD_WIDTH],
playfieldBushes[PLAYFIELD_HEIGHT][PLAYFIELD_WIDTH];


unsigned char   *video_ptr, 
                title_text_color,
                playerX,    // player x position
                playerY,    // player y position
                playerDir,  // player direction (0=N,1=E,2=S,3=W)
                joyDir,     // joy direction
                missleX,    // missle x position
                missleY,    // missle y position
                missleDir,  // missle direction
                canMove,    // notblocked
                fired,      // missle moving = 1, no missle = 0;
                paused,     // game paused
                goalReached=0,
                musicOn=0;

unsigned int    level,
                ammo,
                levelTime,  // remaining time
                startTime,  // start time of level
                fruitsCount,
                bushesCount;

int             entities,   // elements on playfield
                goal,
                exp,
                hiscore=0,
                score;
                
char            txt[40];    // line buffer
char            buf[4];     // int buffer

                
typedef struct {            // counters
    unsigned char fruits;
    unsigned char bushes;
    unsigned char skulls;
    unsigned char ducks;
    unsigned char clocks;
} T_Collectibles;

T_Collectibles collected;       // storage counters
T_Collectibles spawnCounters;   // spawn counters

#endif