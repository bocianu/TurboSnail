#ifndef HELP_H
#define HELP_H


void initHelp(unsigned char page){
	switch (page) {
		case 0:
			pmgColor(SCORES_GOAL);
			playerX = 2;
			playerY = 17;
			putC(27,17,C_EMPTY);
			putC(17,17,C_FRUIT);
			putC(19,17,C_FRUIT1);
			putC(21,17,C_FRUIT2);
			strcpy(&playfield[16][1],"\x26ruits");
		break;
		case 1:
			pmgColor(SCORES_UNDERGOAL);
			putC(17,17,C_BUSH2);
			putC(19,17,C_BUSH1);
			putC(21,17,C_BUSH);
			strcpy(&playfield[16][1],"\x22ushes");
		break;
		case 2:
			pmgColor(SCORES_GOAL);
			putC(19,17,C_DUCK);
			putC(25,17,C_BUSH);
			putC(27,16,C_BUSH1);
			putC(28,18,C_BUSH2);
			strcpy(&playfield[16][1],"\x24ucks");
		break;
		case 3:
			pmgColor(SCORES_UNDERGOAL);
			putC(19,17,C_SKULL);
			putC(25,17,C_FRUIT2);
			putC(27,16,C_FRUIT1);
			putC(28,18,C_FRUIT);
			strcpy(&playfield[16][1],"\x33kulls");
		break;
		case 4:
			pmgColor(SCORES_GOAL);
			putC(19,17,C_CLOCK);
			strcpy(&playfield[16][1],"\x23locks");
		break;
		case 5:
			pmgColor(SCORES_GOAL);
			putC(27,17,C_ROCK2);
			putC(15,17,C_CLOCK);
			putC(17,17,C_FRUIT2);
			putC(19,17,C_BUSH);
			putC(21,17,C_SKULL);
			putC(23,17,C_DUCK);
			strcpy(&playfield[16][1],"\x33hooting");
		break;
	}
	joyDir = 1;	
}

void clearFruits(void){
	showLine(25,16,"     ");
	showLine(25,17,"     ");
	showLine(25,18,"     ");
}

void showHelpPage(unsigned char page){
	switch (page) {
		case 0:
			showLine(2,20,"Fruits are tasty, eat them all!     ");
			showLine(2,21,"score +1, exp +1                    ");
		break;
		case 1:
			showLine(2,20,"Bushes descreases your score.       ");
			showLine(2,21,"score -2                            ");
		break;
		case 2:
			showLine(2,20,"Duck is your friend, gives you exp. ");
			showLine(2,21,"exp +1, also removes some bushes.   ");

		break;
		case 3:
			showLine(2,20,"Avoid skull, it decreases your exp. ");
			showLine(2,21,"score -2, exp -3, removes fruits.   ");
		break;
		case 4:
			showLine(2,20,"Gathering clocks gives extra time.  ");
			showLine(2,21,"time +5 seconds                     ");
		break;
		case 5:
			showLine(2,20,"Ammo can be helpful, but shooting   ");
			showLine(2,21,"bushes and skulls, gives you penalty");
		break;
		
		case 10:
			showLine(2,20,"                                    ");
			showLine(2,21,"                                    ");
		break;
	}
	
}

void showHelp(void) {
    int cycle = 0;
	
    unsigned char 	page = 0,
					cycleCount=0,
					moveSpeed=7;
    title_text_color = 0;
	clearScr;
	clearPlayfield(C_EMPTY);

    POKE(CHBAS,(unsigned int)&(font_data_game)>>8);    // set charset
    
    POKE(710,0);    // set bgcolor
    POKE(709,12);   // set fgcolor
    POKE(752,1);    // hide cursor
    initDLI_normal(); 
    initHelpPMG();
	
	drawBox(0,0,38,22);
	
	showLine(10,2,"instructions:");
	putC(2,2,74);
	putC(3,2,75);
	putC(4,2,76);
	putC(5,2,77);
	putC(6,2,78);
	putC(7,2,79);
	putC(8,2,80);
	
	showLine(2,4,"On every level snail has 30 seconds");
	showLine(2,5,"to eat as many fruits as possible.");
	showLine(2,7,"To pass to the next level you need");
	showLine(2,8,"to get score at least equal or");
	showLine(2,9,"bigger to the level's goal.");
	showLine(2,11,"Score starts from zero on each level");
	showLine(2,12,"but experience is cumulative.");
	showLine(2,13,"Getting it higher and higher");
	showLine(2,14,"is the clue of the game.");
	
	gotoxy(2,21); // fix
	cprintf("\0");
	
	initHelp(page);
    
    while(PEEK(STRIG0)==1 && PEEK(HELPFG)!=17) {

        POKE(CDTMV3,1);
        while(PEEK(CDTMV3)) {
            cycle++;
			if (cycleCount==(moveSpeed>>1) && canMove) playerShow(1); // animate snail

			if (cycleCount==moveSpeed) {	// if enough cycles passed move player
				cycleCount=0;
				playerMove(joyDir);
					
				if (joyDir==1 && playerX==3) showHelpPage(page);

				if (joyDir==1 && playerX==20) {
					memset(&playfield[16][1],C_EMPTY,8);
					if (page==2 || page==3) clearFruits();
				};

				if (playerX==22) joyDir=3;
				
				if (joyDir==3) {
					if (playerX==2) {
						showHelpPage(10);
						page++;
						page = page % 6;
						initHelp(page);
					}
				}
				
				if (joyDir==1 && page==5 && playerX==12 && !fired) {
					clearPlayfield(C_EMPTY);	
					missleX = playerX;
					missleY = playerY;
					missleDir = playerDir;
					fired = 1;
					showHelpPage(page);
				}
				
			}
        }    
		if (fired) missleMove();
		cycleCount++;		
    }
    while(PEEK(STRIG0)==0);
	POKE(HELPFG,0);
	
}

#endif