// ***************************************************************
// ************************** JOYSTICK
// ***************************************************************

unsigned char getJoy(lastDir)
{
	if (PEEK(STICK0) != 15) {		// get joy move
		switch (PEEK(STICK0)) {
			case 14:
				return 0;
			case 7:
				return 1;
			case 13:
				return 2;
			case 11:
				return 3;
			default:
			return lastDir;
		}
	}
	else return lastDir;
}
