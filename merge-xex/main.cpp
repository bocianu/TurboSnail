#include <vector>
#include <stdio.h>

using namespace std;

struct segment {
	size_t begin;
	size_t end;
	unsigned char *data;
};

struct xex_info {
	size_t run_address;
	vector <segment> segments;
};

vector <unsigned char> xex1;
vector <unsigned char> xex2;
xex_info xex1info;
xex_info xex2info;

#define GET_WORD(a) ( (size_t) xex[a] + ((size_t) xex[a+1])*0x100 )

void get_xex_info(vector <unsigned char> &xex, xex_info &info)
{
	size_t check=GET_WORD(0);
	size_t pos;
	size_t start;
	size_t end;
	size_t run;
	if (check!=0xFFFF)
	{
		printf("ERROR: Not a proper xex file!\n");
		exit(2);
	}
	pos=2;
	while(pos<xex.size()-4)
	{	
		start=GET_WORD(pos);
		pos+=2;
		if (start==0xFFFF)
			continue;

		if (start!=0x02E0)
			printf("$%04X",start);

		end=GET_WORD(pos);
		pos+=2;
		if (end!=0x02E1)
		{
			printf("-$%04X : $%04X\n",end,end-start+1);
			if (start>=end)
			{
				printf("start >= end!\n");
				exit(3);
			}
		}

		if (start==0x02E0 && end==0x02e1)
		{
			run=GET_WORD(pos);
			printf("RUN $%04X\n",run);
			info.run_address=run;
		}
		else
		{
			segment s;
			s.begin=start;
			s.end=end;
			s.data=&xex[pos];
			info.segments.push_back(s);
		}
		pos+=end-start+1;
	}
}

void read_file_into_vector(char *filename,vector <unsigned char> &xex)
{
	unsigned char c;
	FILE *fp;
	fp=fopen(filename,"rb");
	if (fp==NULL)
	{
		printf("ERROR: Can't open the %s\n",filename);
		exit(1);
	}
	while(fread(&c,1,1,fp)!=0)
	{
		xex.push_back(c);
	}
	fclose(fp);
}

void check_overleaping(xex_info &info1,xex_info &info2)
{
	for (size_t i=0;i<info1.segments.size();++i)
	{
		for (size_t j=0;j<info2.segments.size();++j)
		{
			if (info1.segments[i].begin>=info2.segments[j].begin && info1.segments[i].begin<=info2.segments[j].end)
			{
				printf("ERROR: xex1 segment start $%04X is in xex2 segment $%04X-$%04X\n",info1.segments[i].begin,info2.segments[j].begin,info2.segments[j].end);
				exit(200);
			}
			if (info1.segments[i].end>=info2.segments[j].begin && info1.segments[i].end<=info2.segments[j].end)
			{
				printf("ERROR: xex1 segment end $%04X is in xex2 segment $%04X-$%04X\n",info1.segments[i].begin,info2.segments[j].begin,info2.segments[j].end);
				exit(201);
			}
		}
	}

}

void merge_xexs(xex_info &info1,xex_info &info2,char *output_name)
{
	FILE *fp=fopen(output_name,"wb+");
	if (fp==NULL)
	{
		printf("Can't open %s !\n",output_name);
		exit(123);
	}

	// header
	size_t data=0xFFFF;
	fwrite(&data,2,1,fp);

	// segments1
	for (size_t i=0;i<info1.segments.size();++i)
	{
		fwrite(&info1.segments[i].begin,2,1,fp);
		fwrite(&info1.segments[i].end,2,1,fp);
		fwrite(info1.segments[i].data,info1.segments[i].end-info1.segments[i].begin+1,1,fp);
	}
	// segments2
	for (size_t i=0;i<info2.segments.size();++i)
	{
		fwrite(&info2.segments[i].begin,2,1,fp);
		fwrite(&info2.segments[i].end,2,1,fp);
		fwrite(info2.segments[i].data,info2.segments[i].end-info2.segments[i].begin+1,1,fp);
	}
	// run
	data=0x02E0;
	fwrite(&data,2,1,fp);
	data=0x02E1;
	fwrite(&data,2,1,fp);
	fwrite(&info2.run_address,2,1,fp);
	fclose(fp);
}


int main(int argc, char *argv[])
{
	if (argc<3)
	{
		printf("Merge-xex by Jakub 'Ilmenit' Debski 2009\n");
		printf("Usage: merge-xex file1.xex file2.xex [output.xex]\nThe RUN address is taken from the second file.\n");
		exit(100);
	}
	read_file_into_vector(argv[1],xex1);
	read_file_into_vector(argv[2],xex2);
	printf("\n%s:\n",argv[1]);
	get_xex_info(xex1,xex1info);
	printf("\n%s:\n",argv[2]);
	get_xex_info(xex2,xex2info);

	char *output_name="output.xex";
	if (argc==4)
		output_name=argv[3];
	check_overleaping(xex1info,xex2info);
	merge_xexs(xex1info,xex2info,output_name);
	return 0;
}