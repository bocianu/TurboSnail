#include <atari.h>
#include <peekpoke.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "atari_defs.h"
#include "globals.h"
#include "blocks.h"
#include "font.h"
#include "pmg.h"
#include "dl.h"
#include "sounds.h"

#include "joystick.c"


// ***************************************************************
// ************************** PLAYFIELD DRAWING ROUTINES
// ***************************************************************

void clearPlayfield(unsigned char sign)
{
	memset(playfield,sign,PLAYFIELD_SIZE);
	memset(playfieldFruits,0,PLAYFIELD_SIZE);
	memset(playfieldBushes,0,PLAYFIELD_SIZE);
}

void showPlayfield(void)
{
	unsigned char b;
	for (b=0;b<PLAYFIELD_HEIGHT;b++)
	{
		memcpy(video_ptr+((b+1)*SCREEN_SIZE)+1,&playfield[b][0],PLAYFIELD_WIDTH);
	}
}


void putEntities(unsigned char count, unsigned char entity)
{
	unsigned char x,y,b;

	for (b=0;b<count;b++)
	{
		if (entities<sizeof(playfield)) {
			do{
				x=rand() % PLAYFIELD_WIDTH;
				y=rand() % PLAYFIELD_HEIGHT;
			}
			while(playfield[y][x]!=C_EMPTY && !(playerX==(x+1) && playerY==(y+1)));
			playfield[y][x]=entity;
			putC(x+1,y+1,entity);
			++entities;
			if (entity==C_FRUIT || entity==C_FRUIT1 || entity==C_FRUIT2) {
				playfieldFruits[y][x] = 1;
				++fruitsCount;
			} 
			if (entity==C_BUSH || entity==C_BUSH1 || entity==C_BUSH2) {
				playfieldBushes[y][x] = 1;
				++bushesCount;
			} 
		}
	}
}

void putEntitiesR(unsigned char count, const char alist[], unsigned char size)
{
	unsigned char x,b;
	for (b=0;b<count;b++)
	{
		x = rand() % size;
		putEntities(1,alist[x]);
	}
}

void removeFruits(char num)
{
unsigned char x,y,i;
	for (i=0;i<PLAYFIELD_MIDH;i++){
		for (x=0;x<PLAYFIELD_WIDTH;x++) {
			y = PLAYFIELD_MIDL-i;
			if (playfieldFruits[y][x]) {
				playfield[y][x] = C_EMPTY;
				playfieldFruits[y][x] = 0;
				putC(x+1,y+1,C_EMPTY);
				--num;
				--fruitsCount;
				--entities;
				if (!num || fruitsCount<1) return;
			}
			
			y = PLAYFIELD_MIDH+i;
			if (playfieldFruits[y][x]) {
				playfield[y][x] = C_EMPTY;
				playfieldFruits[y][x] = 0;
				putC(x+1,y+1,C_EMPTY);
				--num;
				--fruitsCount;
				--entities;
				if (!num || fruitsCount<1) return;
			}
			
		}
	}
}

void removeBushes(char num)
{
unsigned char x,y,i;
	for (i=0;i<PLAYFIELD_MIDH;i++){
		for (x=0;x<PLAYFIELD_WIDTH;x++) {
			y = PLAYFIELD_MIDL-i;
			if (playfieldBushes[y][x]) {
				playfield[y][x] = C_EMPTY;
				playfieldBushes[y][x] = 0;
				putC(x+1,y+1,C_EMPTY);
				--num;
				--bushesCount;
				--entities;
				if (!num || bushesCount<1) return;
			}
			
			y = PLAYFIELD_MIDH+i;
			if (playfieldBushes[y][x]) {
				playfield[y][x] = C_EMPTY;
				playfieldBushes[y][x] = 0;
				putC(x+1,y+1,C_EMPTY);
				--num;
				--bushesCount;
				--entities;
				if (!num || bushesCount<1) return;
			}
			
		}
	}
}

// ***************************************************************
// ************************** SCORING DRAWING
// ***************************************************************

void showNum(char x, char y, int num) // prints integer
{
	gotoxy(x,y);
	cprintf("    ");
	gotoxy(x,y);
	cprintf("%d",num);
}

void updateTime(time) {
	showNum(35,22,time);
}

void updateAmmo(void) {
	showNum(35,20,ammo);
}

void updateHiScore(void){
	showNum(35,14,hiscore);
}

void updateScore(void) {
	showNum(36,8,score);
	if (score<goal) {
		POKE(PCOLR0,SCORES_UNDERGOAL);
		POKE(PCOLR1,SCORES_UNDERGOAL);
		if (goalReached) {
			goalReached=0;
			play_sfx(SFX_NOGOAL,2);
		}
	} else if (goal>0) {
		POKE(PCOLR0,SCORES_GOAL);
		POKE(PCOLR1,SCORES_GOAL);
		if (!goalReached) {
			goalReached=1;
			play_sfx(SFX_GOAL,2);
		}		
	}
}

void updateGoal(void) {
	showNum(36,10,goal);
}

void updateExp(void) {
	showNum(36,6,exp);

}

void updateLevel(void) {
	showNum(36,4,level);
}

// ***************************************************************
// ************************** BOARD DRAWING ROUTINES
// ***************************************************************

void drawBox(unsigned char x,unsigned char y,unsigned char w,unsigned char h)
{
	unsigned char b;
	for(b=y+1;b<=h+y;b++)
	{
		memset(video_ptr+(b*SCREEN_SIZE)+x+1,C_EMPTY,w); // clear contents
		if (b==y+1) {
			putC(x,b,BOXLINE_VLF);
			putC(x+w+1,b,BOXLINE_VRF);
		} else if (b==h+y) {
			putC(x,b,BOXLINE_VLL);
			putC(x+w+1,b,BOXLINE_VRL);
		} else {
			putC(x,b,BOXLINE_VL);
			putC(x+w+1,b,BOXLINE_VR);
		}
	}
	for(b=x+1;b<=w+x;b++)
	{
		if (b&1) {
			putC(b,y,BOXLINE_HT1);
			putC(b,y+h+1,BOXLINE_HB1);
		} else {
			putC(b,y,BOXLINE_HT2);
			putC(b,y+h+1,BOXLINE_HB2);
		}

	}
	putC(x,y,CORNER_TL);			// put corners
	putC(x,y+h+1,CORNER_BL);
	putC(x+w+1,y,CORNER_TR);
	putC(x+w+1,y+h+1,CORNER_BR);
}

void drawBoard(void)
{
	putC(30,1,74);
	putC(31,1,75);
	putC(32,1,76);
	putC(33,1,77);
	putC(34,1,78);
	putC(35,1,79);
	putC(36,1,80);
	
	gotoxy(29,4);			// show counters
	cprintf("Level:");
	gotoxy(29,6);
	cprintf("Exp:");
	gotoxy(29,8);		
	cprintf("Score:");
	gotoxy(29,10);
	cprintf("Goal:");
	gotoxy(29,14);
	cprintf("Best:");
	gotoxy(29,20);
	cprintf("Ammo:");
	gotoxy(29,22);
	cprintf("Time:");

	updateTime(levelTime);			// update counter values
	updateScore();
	updateGoal();
	updateExp();
	updateHiScore();
	updateLevel();
	updateAmmo();

	drawBox(0,0,PLAYFIELD_WIDTH,PLAYFIELD_HEIGHT);
}

// ***************************************************************
// ************************** PLAYER ROUTINES
// ***************************************************************

unsigned char canEnter(unsigned char x,unsigned char y) {
	if (x<1) return 0;
	if (y<1) return 0;
	if (y>PLAYFIELD_HEIGHT) return 0;
	if (x>PLAYFIELD_WIDTH) return 0;
	if (playfield[y-1][x-1] == C_ROCK) return 0;
	if (playfield[y-1][x-1] == C_ROCK2) return 0;
	
	return 1;
}

void playerStep(unsigned char joyDir){
	playerDir = joyDir;
	canMove = 0;
	switch (playerDir) {
		case 0: // step up
			if (canEnter(playerX,playerY-1)) {
				--playerY;
				canMove = 1;
			}
		break;
		case 1: // step right
			if (canEnter(playerX+1,playerY)) {
				++playerX;
				canMove = 1;
			}
		break;
		case 2: // step down
			if (canEnter(playerX,playerY+1)) {
				++playerY;
				canMove = 1;
			}
		break;
		case 3: // step left
			if (canEnter(playerX-1,playerY)) {
				--playerX;
				canMove = 1;
			}
		break;
	}
	if (canMove && !musicOn) play_sfx(SFX_MOVE,1);
}

void playerShow(unsigned char state){

	putC(playerX,playerY,snail[state][playerDir]);	// draw snail

	if (playerDir==1 && canEnter(playerX-1,playerY)) {							// and tail ;)
		putC(playerX-1,playerY,STAIL_E);
	}
	if (playerDir==3 && canEnter(playerX+1,playerY)) {
		putC(playerX+1,playerY,STAIL_W);
	}
}

void playerHide(void){
	putC(playerX,playerY,playfield[playerY-1][playerX-1]);
	if (playerDir==1 && canEnter(playerX-1,playerY)) {
		putC(playerX-1,playerY,playfield[playerY-1][playerX-2]);
	}
	if (playerDir==3 && canEnter(playerX+1,playerY)) {
		putC(playerX+1,playerY,playfield[playerY-1][playerX]);
	}
}

void playerMove(dir) {	// moves player on screen
	playerHide();
	playerStep(dir);
	playerShow(0);
}

void getPointsFor(unsigned char entity) {
	switch (entity) {
		case C_SKULL:	// SKULL
			collected.skulls++;
			score-=2;
			updateScore();
			exp-=3;
			updateExp();
			break;
		case C_BUSH:	// BUSHES
		case C_BUSH1:
		case C_BUSH2:
			play_sfx(SFX_BUSH,0);
			playfieldBushes[playerY-1][playerX-1]=0;
			++collected.bushes;
			--bushesCount;
			score-=2;
			updateScore();
			break;
		case C_FRUIT:	// FRUITS
		case C_FRUIT1:
		case C_FRUIT2:
			play_sfx(SFX_FRUIT,0);
			playfieldFruits[playerY-1][playerX-1]=0;
			++collected.fruits;
			--fruitsCount;
			score+=1;
			updateScore();
			exp+=1;
			updateExp();
			break;
		case C_DUCK:	// DUCK
			collected.ducks++;
		case C_ROCK:	// ROCK
		case C_ROCK2:	// ROCK
			exp+=1;
			updateExp();
			break;
		case C_CLOCK:
			collected.clocks++;
			levelTime += 5;
			updateTime(levelTime);
			play_sfx(SFX_CLOCK,0);
			break;
	}
}
	
void respawnWhenPicked(unsigned char entity) {
	switch (entity) {
		case C_SKULL:	// SKULL
			play_sfx(SFX_SKULL,0);
			removeFruits(level);
			break;
		case C_DUCK:	// DUCK
			play_sfx(SFX_DUCK,0);
			removeBushes(level);
			break;
		case C_FRUIT:	// FRUITS
		case C_FRUIT1:
		case C_FRUIT2:
			++ammo;
			updateAmmo();
			putEntitiesR(1,bushes,3);
			putEntitiesR(1,fruits,3);
			break;
		case C_BUSH:	// BUSHES
		case C_BUSH1:
		case C_BUSH2:
			putEntitiesR(1,bushes,3);
			putEntitiesR(1,fruits,3);
			break;
	}
}
	
void playerCheck(void) {	// check if player stepped on anything
	unsigned char cell = playfield[playerY-1][playerX-1];
	if (cell!=C_EMPTY) {
		getPointsFor(cell);
		respawnWhenPicked(cell);
		--entities;
		playfield[playerY-1][playerX-1]=C_EMPTY; // clear cell
	}
}

// ***************************************************************
// ************************** MISSLE
// ***************************************************************

void playerFired(void) {
	if (ammo>0) {
		--ammo;
		updateAmmo();
		missleX = playerX;
		missleY = playerY;
		missleDir = playerDir;
		fired = 1;
		play_sfx(SFX_SHOOT,2);
	}
}

void missleMove(void) {
	unsigned char cell;
	if (missleX!=playerX || missleY!=playerY) {
		putC(missleX,missleY,playfield[missleY-1][missleX-1]);
	}
	switch (missleDir) {
		case 0: // step up
			if (missleY>1) {
				--missleY;
			} else {
				fired = 0;
			}
		break;
		case 1: // step right
			if (missleX<PLAYFIELD_WIDTH) {
				++missleX;
			} else {
				fired = 0;
			}
		break;
		case 2: // step down
			if (missleY<PLAYFIELD_HEIGHT) {
				++missleY;
			} else {
				fired = 0;
			}
		break;
		case 3: // step left
			if (missleX>1) {
				--missleX;
			} else {
				fired = 0;
			}
		break;
	}
	if (fired) {
		cell = playfield[missleY-1][missleX-1];
		if (cell != C_EMPTY) {
			if (cell == C_ROCK2) {
				fired = 0;
				return;
			}
			getPointsFor(cell);
			--entities;
			playfield[missleY-1][missleX-1] = C_EMPTY;
		}
		putC(missleX,missleY,MISSLE);
	}
}

// ***************************************************************
// ************************** GAME FLOW
// ***************************************************************

void resetGlobals(void)
{
	playerX = PLAYFIELD_WIDTH >> 1;
	playerY = PLAYFIELD_HEIGHT >> 1;
	playerDir = 1;
	joyDir = playerDir;
	levelTime = 30;
	startTime = getRTC;
	score = 0;
	entities = 0;
	ammo = 0;
	goal = 0;
	collected.skulls = 0;
	collected.fruits = 0;
	collected.bushes = 0;
	collected.ducks = 0;
	collected.clocks = 0;
	spawnCounters.skulls = 0;
	spawnCounters.fruits = 0;
	spawnCounters.bushes = 0;
	spawnCounters.ducks = 0;
	spawnCounters.clocks = 0;
	fired = 0;
	paused = 0;
	fruitsCount=0;
	bushesCount=0;
}

void initGame(void){
	score = 0;
	level = 1;
	exp = 0;
	resetGlobals();
	drawBoard();
}

void initLevel(void){												// LEVEL INIT
	unsigned char	mx = PLAYFIELD_WIDTH>>1,
					my = PLAYFIELD_HEIGHT>>1,
					blink =1;
					
	resetGlobals();
	goalReached=0;
	goal = (level<3)?3:(level + 1);		// set goal
	
	drawBox(mx-7,my-3,14,5);			// draw box
	POKE(PCOLR2,BOXBG_LEVELSTART);		// and background
	POKE(PCOLR3,BOXBG_LEVELSTART);
	memset(pmg_data+0x300+52,0xff,20);
	memset(pmg_data+0x380+52,0xff,20);

	gotoxy(mx-3,my-1);
	cprintf("LEVEL %02d",level);
	
	play_music(0x17);

	while(PEEK(STRIG0)==1){				// blink and wait for fire
		if (!PEEK(CDTMV3)) {
			if (blink) {
				gotoxy(mx-4,my+1);
				cprintf("GET READY!");
				blink = 0;
				POKE(CDTMV3,20);
			} else {
				gotoxy(mx-4,my+1);
				cprintf("          ");
				blink = 1;
				POKE(CDTMV3,20);
			}
		}
	};
	while(PEEK(STRIG0)==0);
	
	srand((PEEK(18)*65536+PEEK(19)*256+PEEK(20))); // initialize random generator

	POKE(PCOLR2,0);		// clear box background
	POKE(PCOLR3,0);
	memset(pmg_data+0x300,0,256);
	
	clearPlayfield(C_EMPTY);					// starting entities
	putEntitiesR((level<2)?0:level+1,rocks,2);
	putEntitiesR(1,bushes,3);
	putEntitiesR(1,fruits,3);

	showPlayfield();	// prepare screen
	updateGoal();
	updateScore();
	updateLevel();
	updateAmmo();
	playerShow(0);
	POKE(ATTRACT,0);
	startTime = getRTC;
}

void updateSpawnCounters(void) {
	if (++spawnCounters.skulls==10) {
		spawnCounters.skulls=0;
		putEntities(1,C_SKULL);
	}
	if (++spawnCounters.ducks==15) {
		spawnCounters.ducks=0;
		putEntities(1,C_DUCK);
	}
	if (++spawnCounters.fruits==25) {
		spawnCounters.fruits=0;
		putEntitiesR((level<4)?0:level,fruits,3);
	}
	if (++spawnCounters.bushes==20) {
		spawnCounters.bushes=0;
		putEntitiesR((level<4)?0:level,bushes,3);
	}
	if (++spawnCounters.clocks==10) {
		spawnCounters.clocks=0;
		putEntities((level<3)?0:1,C_CLOCK);
	}
}

void showStatLine(unsigned char x,unsigned char y, unsigned char num, unsigned char symbol) {
	gotoxy(x,y);
	cprintf("  x %02d",num);
	putC(x,y,symbol);
}

unsigned char endOfTime(void) {									// END OF TIME
	unsigned char	mx = PLAYFIELD_WIDTH>>1,
					my = PLAYFIELD_HEIGHT>>1,
					lost = 0,
					blink = 0;
                    
	if (exp > hiscore) {                                    // hiscore update
		hiscore = exp;
		updateHiScore();
	}                    

	drawBox(mx-7,my-7,14,14);					
	lost = ((score < goal)?1:0);
	
	POKE(PCOLR2,lost?BOXBG_GAMEOVER:BOXBG_WELLDONE);		// colour background
	POKE(PCOLR3,lost?BOXBG_GAMEOVER:BOXBG_WELLDONE);
	memset(pmg_data+0x300+36,0xff,56);
	memset(pmg_data+0x380+36,0xff,56);

	stop_music();
	if (lost) {
		play_music(0x1a);
	} else {
		play_music(0x1d);
	}
	

	showStatLine(mx-2,my-3,collected.fruits,C_FRUIT);		// show results
	showStatLine(mx-2,my-1,collected.bushes,C_BUSH);
	showStatLine(mx-2,my+1,collected.skulls,C_SKULL);
	showStatLine(mx-2,my+3,collected.ducks,C_DUCK);
	showStatLine(mx-2,my+5,collected.clocks,C_CLOCK);
	
	while(PEEK(STRIG0)==1){									// blink and wait
		if (!PEEK(CDTMV3)) {
			if (blink) {
				gotoxy(mx-4,my-5);
				cprintf(((lost==1)?"GAME OVER!":"WELL DONE!"));
				blink = 0;
				POKE(CDTMV3,20);
			} else {
				gotoxy(mx-4,my-5);
				cprintf("          ");
				blink = 1;
				POKE(CDTMV3,20);
			}
		}
	}
	while(PEEK(STRIG0)==0);
	
	POKE(PCOLR2,0);											// clear background
	POKE(PCOLR3,0);
	memset(pmg_data+0x300,0,256);

	gotoxy(38,23);  // fix
	cprintf("\0");
	
	showPlayfield();
	return lost;
}

void gamePaused(void){
	
unsigned char	mx = PLAYFIELD_WIDTH>>1,
				my = PLAYFIELD_HEIGHT>>1,
				blink = 0;

	while(PEEK(CONSOL)==3);
	drawBox(mx-7,my-1,14,3);			// draw box
	POKE(PCOLR2,BOXBG_LEVELSTART);		// and background
	POKE(PCOLR3,BOXBG_LEVELSTART);
	memset(pmg_data+0x300+60,0xff,12);
	memset(pmg_data+0x380+60,0xff,12);

	while(PEEK(STRIG0)==1 && PEEK(CONSOL)!=3){				// blink and wait for fire
		if (!PEEK(CDTMV3)) {
			if (blink) {
				gotoxy(mx-5,my+1);
				cprintf("GAME PAUSED!");
				blink = 0;
				POKE(CDTMV3,20);
			} else {
				gotoxy(mx-5,my+1);
				cprintf("            ");
				blink = 1;
				POKE(CDTMV3,20);
			}
		}
	};
	while(PEEK(STRIG0)==0 || PEEK(CONSOL)==3);
	paused = 0;
	POKE(PCOLR2,0);											// clear background
	POKE(PCOLR3,0);
	memset(pmg_data+0x300,0,256);

	showPlayfield();	
}

#include "titlescr.c"
#include "helpscr.c"

void gameStart(void) {

	unsigned char	cycleCount=0,
					moveSpeed=8,
					gameOver=0;

	clearScr;					
	POKE(710,0);
	initDLI_normal();
	
	POKE(CHBAS,(unsigned int)&font_data_game>>8);
	initGamePMG();
	initGame();
	initLevel();

	do // ****************************************** GAME LOOP
	{

		if (paused) gamePaused();
	
		if ( startTime != getRTC ) {	// calc time
			updateSpawnCounters();
			startTime = getRTC;
			updateTime(--levelTime);
			if (levelTime<=0) {			// end of time
				gameOver = endOfTime();
				if (!gameOver) {
					cycleCount = 0;
					level++;
					initLevel();
				}
			}
		}

		POKE(CDTMV3,1);			// wait one cycle
		while(PEEK(CDTMV3)) {
			joyDir = getJoy(joyDir);  // read joy
			if (PEEK(STRIG0)==0 && fired==0) {
				playerFired();
			};
			if (PEEK(CONSOL)==3) paused=1;
		}
		
		if (fired) missleMove();			// animate missle
		
		if (cycleCount==(moveSpeed>>1) && canMove) playerShow(1); // animate snail

		if (cycleCount==moveSpeed) {	// if enough cycles passed move player
			cycleCount=0;
			playerMove(joyDir);
			playerCheck();
		}

		cycleCount++;
	
	}while(!gameOver);
	// ***************************************** END OF GAME LOOP

}

// ***************************************************************
// ************************** MAIN
// ***************************************************************

int main (void)
{

	unsigned char action = GAME_TITLE;
	_graphics(0);	
	video_ptr=(unsigned char*)(PEEKW(PEEKW(560)+4));
	init_sfx();

	while (action!=GAME_EXIT) {
		action = showTitleScreen();
		switch (action) {
			case GAME_START:
				gameStart();
				break;
			case GAME_HALL_OF:
				//showHallOfFame(); // TODO
				break;
			case GAME_HELP:
				showHelp(); 
				break;
		}
	}
	return EXIT_SUCCESS;
}


