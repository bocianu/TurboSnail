#include <atari.h>
#include <stdio.h>
#include <peekpoke.h>
#include "sounds.h"
#include "sfx\sfx.h"

// DISABLE OPTIMIZATION!!!, we do some hacks here!
// It will crash if the function get f.e. inlined.

#pragma optimize (off)

void play_sfx(unsigned char effect_number,unsigned char track)
{
	POKE(SFX_TRACK_TO_PLAY,track);
	if (effect_number!=0)
		POKE(SFX_SFX_EFFECT,effect_number);
}

void stop_music(void)
{
	__asm__ ("jsr %w",SFX_RMT_SILENCE);
	play_music(0);
}

void play_music(unsigned char track_number)
{
	POKE(SFX_TRACK_TO_PLAY,track_number);
	__asm__ ("jsr %w",SFX_START);
}

void init_sfx(void)
{
	POKE(SFX_MUSIC_PLAYS,1);
	__asm__ ("jsr %w",SFX_START);
}



#pragma optimize (on)
