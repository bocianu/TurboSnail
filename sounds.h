#ifndef SOUNDS_H
#define SOUNDS_H

void init_sfx(void);
void play_sfx(unsigned char effect,unsigned char track);
void play_music(unsigned char track_number);
void stop_music(void);

#endif
