  .export _dli1
  .import _font_data_main
  .import _title_text_color
COLPF1  =   $D017
COLPF2  =   $D018
WSYNC   =   $D40A
VDSLSTL = 	$0200
VDSLSTH = 	$0201
CHBASE	=	$D409


  .code
_dli1:           pha                     ; save registers
  lda #>_font_data_main
  sta WSYNC
  sta CHBASE
  lda _title_text_color
  sta COLPF1
  lda #<_dli2
  sta VDSLSTL
  lda #>_dli2
  sta VDSLSTH
  pla                     ; restore registers
  rti
  
_dli2:           pha                     ; save registers
  lda #4
  sta WSYNC
  sta COLPF1
  lda #<_dli1
  sta VDSLSTL
  lda #>_dli1
  sta VDSLSTH
  pla                     ; restore registers
  rti  